const fs = require("fs");
const fsPromises = require("fs").promises;

function problem2() {
  fsPromises
    .readFile("../lipsum_2.txt", "utf-8")
    .then((data) => {
      return data.toUpperCase();
    })
    .then((data) => {
      let upperCasePath = "../upperCaseLipsum.txt";
      fsPromises.writeFile(upperCasePath, data, "utf-8");
      console.log("upperCase  file is created successfully");
      return upperCasePath;
    })
    .then((filepath) => {
      let storePath = "../filenames.txt";
      fsPromises.writeFile(storePath, filepath, "utf-8");

      return filepath;
    })
    .then((tolowerCase) => {
      return fsPromises.readFile(tolowerCase, "utf-8");
    })
    .then((data) => {
      let lowerData = data.toLowerCase().split(".");

      let lowerPath = "../lowerCaseLipsum.txt";
      fsPromises.writeFile(lowerPath, JSON.stringify(lowerData));
      console.log(`lowerCase data file created ${lowerPath}`);
      fsPromises.appendFile("../filenames.txt", `\n${lowerPath}`);
      console.log(`${lowerPath} file names stored in filenames.txt`);
      return lowerPath;
    })
    .then((readPath) => {
      return fsPromises.readFile(readPath, "utf-8");
    })
    .then((sortData) => {
      let parseData = JSON.parse(sortData);
      let sortedData = parseData.sort();
      //console.log(sortedData,"here")
      let sortedDataPath = "../sortedLipsumData.txt";
      fsPromises.writeFile(sortedDataPath, JSON.stringify(sortedData));
      console.log(`sorted data sotord file created ${sortedDataPath}`);
      fsPromises.appendFile("../filenames.txt", `\n${sortedDataPath}`);
      console.log(`${sortedDataPath} added to filenames.txt file`);
    })
    .then(() => {
      return fsPromises.readFile("../filenames.txt", "utf-8");
    })
    .then((fileNames) => {
      let filesNamesData = fileNames.split("\n");

      for (let paths of filesNamesData) {
        fsPromises.unlink(paths);
        console.log(`${paths} file is deleted successfully`);
      }
    })
    .catch((error) => {
      console.log(error);
    });
}
//problem2();
module.exports = problem2;
