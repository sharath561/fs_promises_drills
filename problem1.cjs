const fs = require("fs");
const fsPromises = require("fs").promises;

function createDeleteFiles() {
  let folderName = "random";

  let filesData = [
    `../${folderName}/random1.json`,
    `../${folderName}/random2.json`,
    `../${folderName}/random3.json`,
  ];

  if (!fs.existsSync(folderName)) {
    fsPromises.mkdir(`../${folderName}`);
  }
  for (let path of filesData) {
    fsPromises
      .writeFile(path, JSON.stringify("created random file"), "utf-8")
      .then(() => {
        console.log(`${path} file is created`);
      })
      .then(() => {
        fsPromises.unlink(path).then(() => {
          console.log(` ${path} deleted`);
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }
}

//createDeleteFiles();
module.exports = createDeleteFiles;
